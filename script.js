/*
Задание

Реализовать переключение вкладок (табы) на чистом Javascript.

Технические требования:

В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.*/



let ul = document.getElementById('tabs');
let nodes = Array.from(ul.children);
let tabContent = document.querySelectorAll('.tab-content');

nodes.forEach(function(element) {
    element.addEventListener('click', function(e){
        let index = nodes.indexOf(e.target);

        for (let item = 0; item < nodes.length; item++) {
            nodes[item].className = 'tab-links';
            tabContent[item].className = 'tab-content';

            if (index === item) {
                nodes[item].className = 'tab-links active';
                tabContent[item].className = 'tab-content active';
            }
        }
    })
});
